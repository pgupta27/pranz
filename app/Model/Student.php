
<?php
/**
 * Created by PhpStorm.
 * User: Pranz
 * Date: 4/26/16
 * Time: 00:17
 */

class Student extends AppModel {


    public $hasMany = array(
        'Attend' => array(
            'className'  => 'Attend',
            'foreignKey' => 'Student_id',
            'dependent' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
            /*'conditions' => array('Student.id = Attend.student_id')*/
        )
    );


}