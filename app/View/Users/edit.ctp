
<div class="users form">
<?php echo $this->Form->create('User',array('class'=> 'form-horizontal','style'=>"margin: 2%;")); ?>
	<fieldset>
		<legend><b><?php echo __('Edit User'); ?></b></legend>
	<?php
		echo $this->Form->input('name',array('label'=>'Name &nbsp&nbsp', 'class'=>'form'));
      echo $this->Form->input('username',array('label'=>'User Name &nbsp&nbsp', 'class'=>'form'));
      echo $this->Form->input('email',array('label'=>'E-mail &nbsp&nbsp', 'class'=>'form'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>




