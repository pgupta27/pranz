<h1>Students</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Created</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($students as $student): ?>
    <tr>
        <td><?php echo $student['Student']['id']; ?></td>
        <td>
            <?php echo $this->Html->link($student['Student']['name'],
            array('controller' => 'students', 'action' => 'view', $student['Student']['id'])); ?>
        </td>
        <td><?php echo $student['Student']['created']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($student); ?>
</table>

<?php echo $this->Html->link(
'Add Student',
array('controller' => 'students', 'action' => 'add')
); ?>