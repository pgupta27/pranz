<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel
{

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'User';

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';


    // The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */


//Login begins


    public $validate = array(
        'name' => array(
            'Please enter your name.' => array(
                'rule' => 'notBlank',
                'message' => 'Please enter your name.'
            )
        ),
        'username' => array(
            'The username must be between 5 and 15 characters.' => array(
                'rule' => array('between', 5, 15),
                'message' => 'The username must be between 5 and 15 characters.'
            ),
            'That username has already been taken' => array(
                'rule' => 'isUnique',
                'message' => 'That username has already been taken.'
            )
        ),
        'email' => array(
            'Valid email' => array(
                'rule' => array('email'),
                'message' => 'Please enter a valid email address'
            )
        ),
        'password' => array(
            'Not empty' => array(
                'rule' => 'notBlank',
                'message' => 'Enter your password'
            ),
            'Match passwords' => array(
                'rule' => 'matchPasswords',
                'message' => 'Passwords do not match'
            )
        ),
        'password_confirmation' => array(
            'Not empty' => array(
                'rule' => 'notBlank',
                'message' => 'Confirm your password'
            )
        )
    );

    public function matchPasswords($data)
    {
        if ($data['password'] == $this->data['User']['password_confirmation']) {
            return true;
        }
        $this->invalidate('password_confirmation', 'Passwords do not match');
        return false;
    }

    public function beforeSave($options = array())
    {
        if (isset($this->data['User']['password'])) {
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        }
        return true;
    }
}
//login ends