<?php
/**
 * Created by PhpStorm.
 * User: Pranz
 * Date: 4/26/16
 * Time: 00:22
 */
App::uses('AppController', 'Controller');

class StudentsController extends AppController {
    public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('students', $this->Student->find('all'));
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid entry'));
        }

        $student = $this->Student->findById($id);
        if (!$student) {
            throw new NotFoundException(__('Invalid entry'));
        }
        $this->set('student', $student);
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Student->create();
            if ($this->Student->save($this->request->data)) {
                $this->Flash->success(__('Your entry has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your entry.'));
        }

    }

}