<h1>Attendance</h1>
<table>
    <tr>
        <th>Id</th>
        <th>Student ID</th>
        <th>Student Name</th>
        <th>Date</th>
        <th>Attendance</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($attends as $attend): ?>
    <tr>
        <td><?php echo $attend['Attend']['id']; ?></td>
        <td><?php echo $attend['Attend']['student_id']; ?></td>
        <td>
            <?php echo $this->Html->link($attend['Student']['name'],
            array('controller' => 'students', 'action' => 'view', $attend['Student']['id'])); ?>
        </td>
        <td><?php echo $attend['Attend']['date']; ?></td>
        <td><?php echo $attend['Attend']['attendance']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($student); ?>
</table>

<?php echo $this->Html->link(
'Add Attendance',
array('controller' => 'attends', 'action' => 'add')
); ?>
<!--
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Attendance</th>
    </tr>

    &lt;!&ndash; Here is where we loop through our $posts array, printing out post info &ndash;&gt;

    <?php foreach ($students as $student): ?>
    <tr>
        <td><?php echo $student['Student']['id']; ?></td>
        <td>
            <?php echo $this->Html->link($student['Student']['name'],
            array('controller' => 'students', 'action' => 'view', $student['Student']['id'])); ?>
        </td>
        <td>$form->input('attend_id',array('type'=>'select'));</td>
    </tr>
    <?php endforeach; ?>
    <?php unset($student); ?>
</table>
-->


