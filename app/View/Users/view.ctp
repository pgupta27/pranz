
	<div class="users view">
		<h2><?php echo __('User'); ?></h2>
		<table class="table">
			<tr>
				<td class="active" width="15%" style="font-weight: bold"><?php echo __('Id'); ?></td>
				<td>
					<?php echo h($user['User']['id']); ?>
					&nbsp;
				</td>
				<td class="active" width="15%" style="font-weight: bold"><?php echo __('Name'); ?></td>
				<td>
					<?php echo h($user['User']['name']); ?>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td class="active" width="15%" style="font-weight: bold"><?php echo __('Username'); ?></td>
				<td>
					<?php echo h($user['User']['username']); ?>
					&nbsp;
				</td>
				<td class="active" width="15%" style="font-weight: bold"><?php echo __('Email'); ?></td>
				<td>
					<?php echo h($user['User']['email']); ?>
					&nbsp;
				</td>
			</tr>
			<tr>

				<!--<dt><?php echo __('Password'); ?></dt>
                  <dd>
                     <?php echo h($user['User']['password']); ?>
                     &nbsp;
                  </dd>
                  <dt><?php echo __('Role'); ?></dt>
                  <dd>
                     <?php echo h($user['User']['role']); ?>
                     &nbsp;
                  </dd>-->
				<td class="active" width="15%" style="font-weight: bold"><?php echo __('Created'); ?></td>
				<td>
					<?php echo h($user['User']['created']); ?>
					&nbsp;
				</td>
				<td class="active" width="15%" style="font-weight: bold"><?php echo __('Modified'); ?></td>
				<td>
					<?php echo h($user['User']['modified']); ?>
					&nbsp;
				</td>
			</tr>
		</table>

	</div>
