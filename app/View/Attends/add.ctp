
<div class="users form">
    <?php echo $this->Form->create('Attend', array('class'=> 'form-horizontal','style'=>"margin: 2%;"));?>
    <fieldset>
        <legend><b>Attendance</b></legend>
        <?php
		echo $this->Form->input('student_id',array('label'=>'Student Name &nbsp&nbsp', 'class'=>'form'));
        echo $this->Form->input('date', array('label'=>'Date' , 'type' => 'date',
        'dateFormat' => 'DMY'
        ));
        echo $this->Form->input('attendance',array(
        'label'=>'Attendance &nbsp&nbsp', 'class'=>'form',
        '1' => 'Present',
        '2' => 'Absent',
        'options' => array('Present', 'Absent')
        ));

       ?>
    </fieldset>
    <?php echo $this->Form->end('Submit');?>
</div>

<!--login ends-->
