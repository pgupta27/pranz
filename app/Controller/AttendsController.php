<?php
/**
 * Created by PhpStorm.
 * User: Pranz
 * Date: 4/26/16
 * Time: 01:40
 */

App::uses('AppController', 'Controller');

class AttendsController extends AppController
{

    public $components = array('Paginator', 'Flash', 'Session');

    public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('attends', $this->Attend->find('all'));

    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid entry'));
        }

        $attend = $this->Attend->findById($id);
        if (!$attend) {
            throw new NotFoundException(__('Invalid entry'));
        }
        $this->set('attend', $attend);
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Attend->create();
            if ($this->Attend->save($this->request->data)) {
                $this->Flash->success(__('Your entry has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your entry.'));
        }

        $students = $this->Attend->Student->find('list');
        $this->set(compact('students'));
    }


}

