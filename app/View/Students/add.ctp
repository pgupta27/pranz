


<div class="users form">
    <?php echo $this->Form->create('Student', array('class'=> 'form-horizontal','style'=>"margin: 2%;"));?>
    <fieldset>
        <legend><b>Enter Student Details</b></legend>
        <?php
		echo $this->Form->input('name',array('label'=>'Name &nbsp&nbsp', 'class'=>'form'));
        echo $this->Form->input('details',array('rows'=> '3', 'label'=>'Student details &nbsp&nbsp', 'class'=>'form'));
         ?>
    </fieldset>
    <?php echo $this->Form->end('Submit');?>
</div>